# If any of the following macros should be set otherwise,
# you can wrap any of them with the following conditions:
# - %%if 0%%{centos} == 7
# - %%if 0%%{?rhel} == 7
# - %%if 0%%{?fedora} == 23
# Or just test for particular distribution:
# - %%if 0%%{centos}
# - %%if 0%%{?rhel}
# - %%if 0%%{?fedora}
#
# Be aware, on centos, both %%rhel and %%centos are set. If you want to test
# rhel specific macros, you can use %%if 0%%{?rhel} && 0%%{?centos} == 0 condition.
# (Don't forget to replace double percentage symbol with single one in order to apply a condition)

# Loop with this package.
%global with_shiny 0

# Generate devel rpm
%global with_devel 1
# Build project from bundled dependencies
%global with_bundled 0
# Build with debug info rpm
%global with_debug 0
# Run tests in check section
%global with_check 1
# Generate unit-test rpm
%global with_unit_test 1

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif


%global provider        github
%global provider_tld    com
%global project         golang
%global repo            exp
# https://github.com/golang/exp
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     golang.org/x/exp
%global commit          50c28f97489115b511c3104aafe83f148deb39a9
%global shortcommit     %(c=%{commit}; echo ${c:0:7})
%global commitdate      20170905

Name:           golang-%{provider}-%{project}-%{repo}
Version:        0
Release:        0.1.%{commitdate}git%{shortcommit}%{?dist}
Summary:        Experimental and deprecated packages
License:        BSD
URL:            https://%{provider_prefix}
Source0:        https://%{provider_prefix}/archive/%{commit}/%{repo}-%{shortcommit}.tar.gz

# e.g. el6 has ppc64 arch without gcc-go, so EA tag is required
ExclusiveArch:  %{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 aarch64 %{arm}}
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}

%description
The idea for this subrepository originated as the "pkg/exp" directory of the
main repository, and holds experimental and deprecated (in the "old" directory)
packages.

Warning: Packages here are experimental and unreliable. Some may one day be
promoted to the main repository or other subrepository, or they may be modified
arbitrarily or even disappear altogether.

In short, code in this subrepository is not subject to the Go 1
compatibility promise. (No subrepo is, but the promise is even more
likely to be violated by go.exp than the others.)

Caveat emptor.


%if 0%{?with_devel}
%package devel
Summary:       %{summary}
BuildArch:     noarch

%if 0%{?with_check} && ! 0%{?with_bundled}
BuildRequires: golang(github.com/BurntSushi/xgb)
BuildRequires: golang(github.com/BurntSushi/xgb/render)
BuildRequires: golang(github.com/BurntSushi/xgb/shm)
BuildRequires: golang(github.com/BurntSushi/xgb/xproto)
BuildRequires: golang(golang.org/x/image/font)
BuildRequires: golang(golang.org/x/image/font/inconsolata)
BuildRequires: golang(golang.org/x/image/math/f32)
BuildRequires: golang(golang.org/x/image/math/f64)
BuildRequires: golang(golang.org/x/image/math/fixed)
BuildRequires: golang(golang.org/x/image/vector)
%if %{with_shiny}
BuildRequires: golang(golang.org/x/mobile/event/key)
BuildRequires: golang(golang.org/x/mobile/event/lifecycle)
BuildRequires: golang(golang.org/x/mobile/event/mouse)
BuildRequires: golang(golang.org/x/mobile/event/paint)
BuildRequires: golang(golang.org/x/mobile/event/size)
BuildRequires: golang(golang.org/x/mobile/geom)
BuildRequires: golang(golang.org/x/mobile/gl)
BuildRequires: golang(golang.org/x/sys/windows)
%endif
BuildRequires: /usr/lib/golang/doc/go_spec.html
%endif

Requires:      golang(github.com/BurntSushi/xgb)
Requires:      golang(github.com/BurntSushi/xgb/render)
Requires:      golang(github.com/BurntSushi/xgb/shm)
Requires:      golang(github.com/BurntSushi/xgb/xproto)
Requires:      golang(golang.org/x/image/font)
Requires:      golang(golang.org/x/image/font/inconsolata)
Requires:      golang(golang.org/x/image/math/f32)
Requires:      golang(golang.org/x/image/math/f64)
Requires:      golang(golang.org/x/image/math/fixed)
Requires:      golang(golang.org/x/image/vector)
%if %{with_shiny}
Requires:      golang(golang.org/x/mobile/event/key)
Requires:      golang(golang.org/x/mobile/event/lifecycle)
Requires:      golang(golang.org/x/mobile/event/mouse)
Requires:      golang(golang.org/x/mobile/event/paint)
Requires:      golang(golang.org/x/mobile/event/size)
Requires:      golang(golang.org/x/mobile/geom)
Requires:      golang(golang.org/x/mobile/gl)
Requires:      golang(golang.org/x/sys/windows)
%endif

Provides:      golang(%{import_path}/ebnf) = %{version}-%{release}
Provides:      golang(%{import_path}/io/i2c) = %{version}-%{release}
Provides:      golang(%{import_path}/io/i2c/driver) = %{version}-%{release}
Provides:      golang(%{import_path}/io/spi) = %{version}-%{release}
Provides:      golang(%{import_path}/io/spi/driver) = %{version}-%{release}
Provides:      golang(%{import_path}/mmap) = %{version}-%{release}
Provides:      golang(%{import_path}/old/netchan) = %{version}-%{release}
Provides:      golang(%{import_path}/rand) = %{version}-%{release}
%if %{with_shiny}
Provides:      golang(%{import_path}/shiny/driver) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/driver/gldriver) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/driver/windriver) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/driver/x11driver) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/gesture) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/iconvg) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/imageutil) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/materialdesign/colornames) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/materialdesign/icons) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/screen) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/text) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/unit) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/widget) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/widget/flex) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/widget/glwidget) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/widget/node) = %{version}-%{release}
Provides:      golang(%{import_path}/shiny/widget/theme) = %{version}-%{release}
%endif
Provides:      golang(%{import_path}/utf8string) = %{version}-%{release}
Provides:      golang(%{import_path}/winfsnotify) = %{version}-%{release}

%description devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%package unit-test-devel
Summary:         Unit tests for %{name} package
%if 0%{?with_check}
#Here comes all BuildRequires: PACKAGE the unit tests
#in %%check section need for running
%endif

# test subpackage tests code from devel subpackage
Requires:        %{name}-devel = %{version}-%{release}

%if 0%{?with_check} && ! 0%{?with_bundled}
%endif


%description unit-test-devel
%{summary}

This package contains unit tests for project
providing packages with %{import_path} prefix.
%endif

%prep
%autosetup -n %{repo}-%{commit}

%if ! %{with_shiny}
rm -rf shiny
%endif

%build

%install
# source codes for building projects
%if 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
echo "%%dir %%{gopath}/src/%%{import_path}/." >> devel.file-list
# find all *.go but no *_test.go files and generate devel.file-list
for file in $(find . \( -iname "*.go" -or -iname "*.s" \) \! -iname "*_test.go" | grep -v "shiny/vendor") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

# testing files for this project
%if 0%{?with_unit_test} && 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
# find all *_test.go files and generate unit-test-devel.file-list
for file in $(find . -iname "*_test.go" | grep -v "shiny/vendor") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> unit-test-devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

%if 0%{?with_devel}
sort -u -o devel.file-list devel.file-list
%endif

%check
%if 0%{?with_check} && 0%{?with_unit_test} && 0%{?with_devel}
%if ! 0%{?with_bundled}
export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%else

%if %{with_shiny}
export GOPATH=%{buildroot}/%{gopath}:$(pwd)/shiny/vendor:%{gopath}
%else
export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%endif
%endif

%if ! 0%{?gotest:1}
%global gotest go test
%endif

%gotest %{import_path}/ebnf
%gotest %{import_path}/ebnflint
%gotest %{import_path}/io/i2c
%gotest %{import_path}/io/spi
%gotest %{import_path}/mmap
%gotest %{import_path}/old/netchan
%gotest %{import_path}/rand
%if %{with_shiny}
%gotest %{import_path}/shiny/driver/internal/swizzle
%gotest %{import_path}/shiny/example/goban
%gotest %{import_path}/shiny/iconvg
%gotest %{import_path}/shiny/imageutil
%gotest %{import_path}/shiny/materialdesign/colornames
%gotest %{import_path}/shiny/materialdesign/icons
%gotest %{import_path}/shiny/screen
%gotest %{import_path}/shiny/text
%gotest %{import_path}/shiny/widget/flex
%gotest %{import_path}/shiny/widget/theme
%endif
%gotest %{import_path}/utf8string
%endif

#define license tag if not already defined
%{!?_licensedir:%global license %doc}


%if 0%{?with_devel}
%files devel -f devel.file-list
%license LICENSE PATENTS
%doc README AUTHORS CONTRIBUTING.md CONTRIBUTORS
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%files unit-test-devel -f unit-test-devel.file-list
%license LICENSE PATENTS
%doc README AUTHORS CONTRIBUTING.md CONTRIBUTORS
%endif

%changelog
* Thu Sep 21 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0-0.1.20170905git50c28f9
- new package built with tito

* Thu Sep 21 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0-0.1.20170905git50c28f9
- First package for Fedora
