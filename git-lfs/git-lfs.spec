# Generate devel rpm
%global with_devel 0
# Build project from bundled dependencies
%global with_bundled 0
# Build with debug info rpm
%global with_debug 1
# Run tests in check section
%global with_check 1
# Generate unit-test rpm
%global with_unit_test 0

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif


%global provider        github
%global provider_tld    com
%global project         git-lfs
%global repo            git-lfs
# https://github.com/git-lfs/git-lfs
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}
%global commit          621d1f821f73efcedc829dda43fd9c1fcf07c6ab
%global shortcommit     %(c=%{commit}; echo ${c:0:7})

Name:           git-lfs
Version:        2.2.1
Release:        3%{?dist}
Summary:        Git extension for versioning large files

License:        MIT
URL:            https://git-lfs.github.io/
Source0:        https://%{provider_prefix}/archive/v%{version}/%{name}-%{version}.tar.gz

ExclusiveArch:  %{go_arches}
BuildRequires:  compiler(go-compiler)

BuildRequires:  golang(github.com/bgentry/go-netrc/netrc)
BuildRequires:  golang(github.com/kr/pretty)
BuildRequires:  golang(github.com/kr/pty)
BuildRequires:  golang(github.com/kr/text)
BuildRequires:  golang(github.com/inconshreveable/mousetrap)
BuildRequires:  golang(github.com/olekukonko/ts)
BuildRequires:  golang(github.com/rubyist/tracerx)
BuildRequires:  golang(github.com/spf13/cobra)
BuildRequires:  golang(github.com/spf13/pflag)
BuildRequires:  golang(github.com/ThomsonReutersEikon/go-ntlm/ntlm)
BuildRequires:  golang(github.com/xeipuuv/gojsonpointer)
BuildRequires:  golang(github.com/xeipuuv/gojsonreference)
BuildRequires:  golang(github.com/xeipuuv/gojsonschema)
BuildRequires:  golang(github.com/pkg/errors)

# Generate mans
BuildRequires:  /usr/bin/ronn

# For tests
BuildRequires:  golang(github.com/stretchr/testify)
BuildRequires:  git >= 1.8.5

Requires:       git >= 1.8.5

%description
Git Large File Storage (LFS) replaces large files such as audio samples,
videos, datasets, and graphics with text pointers inside Git, while
storing the file contents on a remote server.


%if 0%{?with_devel}
%package -n golang-%{provider}-%{project}-%{repo}-devel
Summary:       %{summary}
BuildArch:     noarch

%description -n golang-%{provider}-%{project}-%{repo}-devel
%{summary}

This package contains library source intended for
building other packages which use import path with
%{import_path} prefix.
%endif


%if 0%{?with_unit_test} && 0%{?with_devel}
%package -n golang-%{provider}-%{project}-%{repo}-unit-test-devel
Summary:         Unit tests for %{name} package

# test subpackage tests code from devel subpackage
Requires:        golang-%{provider}-%{project}-%{repo}-devel = %{version}-%{release}


%description -n golang-%{provider}-%{project}-%{repo}-unit-test-devel
%{summary}

This package contains unit tests for project
providing packages with %{import_path} prefix.
%endif


%prep
%autosetup -p1

# Unbundle libs
rm -rf vendor


%build
mkdir -p src/%{provider}.%{provider_tld}/%{project}/
ln -s $(pwd) src/%{provider}.%{provider_tld}/%{project}/%{repo}
export GOPATH=$(pwd):%{gopath}

%gobuild -o bin/git-lfs %{import_path}

# Build manpages
ronn docs/man/*.ronn


%install
install -Dpm0755 bin/git-lfs %{buildroot}%{_bindir}/%{name}
install -d -p %{buildroot}%{_mandir}/man1/
install -Dpm0644 docs/man/*.1 %{buildroot}%{_mandir}/man1/
install -d -p %{buildroot}%{_mandir}/man5/
install -Dpm0644 docs/man/*.5 %{buildroot}%{_mandir}/man5/

# source codes for building projects
%if 0%{?with_devel}
install -d -p %{buildroot}%{gopath}/src/%{import_path}/
echo "%%dir %%{gopath}/src/%%{import_path}/." >> devel.file-list
# find all *.go but no *_test.go files and generate devel.file-list
for file in $(find . \( -iname "*.go" -or -iname "*.s" \) \! -iname "*_test.go") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

# testing files for this project
%if 0%{?with_unit_test} && 0%{?with_devel}
install -d -p %{buildroot}%{gopath}/src/%{import_path}/
# find all *_test.go files and generate unit-test-devel.file-list
for file in $(find . -iname "*_test.go") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> unit-test-devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

%if 0%{?with_devel}
sort -u -o devel.file-list devel.file-list
%endif


%post
%{_bindir}/%{name} install --system

%preun
if [ $1 -gt 0 ]; then
    %{_bindir}/%{name} uninstall
fi
exit 0


%check
%if 0%{?with_check}
export GOPATH=%{buildroot}%{gopath}:$(pwd):%{gopath}
%gotest %{import_path}/git
%gotest %{import_path}/lfs
%endif


%files
%license LICENSE.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*.1*
%{_mandir}/man5/%{name}*.5*

%if 0%{?with_devel}
%files -n golang-%{provider}-%{project}-%{repo}-devel -f devel.file-list
%license LICENSE.md
%dir %{gopath}/src/%{provider}.%{provider_tld}/%{project}
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%files -n golang-%{provider}-%{project}-%{repo}-unit-test-devel -f unit-test-devel.file-list
%license LICENSE.md
%endif


%changelog
* Fri Sep 01 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.2.1-3
- Remove redundant doc tag on manpages.
- Use path macros in %%post/%%postun.

* Thu Aug 31 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.2.1-2
- Disable unnecessary subpackages.

* Sun Jul 30 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.2.1-1
- Update to latest version.

* Wed Apr 19 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.2-2
- Patch up to build with Go 1.7

* Wed Apr 19 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.2-1
- Update to latest release
- Add some requested macros

* Tue Mar 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.1-1
- Update to latest release
- Don't disable git-lfs globally during upgrade

* Mon Mar 06 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0.0-1
- Update to latest release

* Sun Feb 12 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.5.5-1
- Update to latest release
- Add -devel and -unit-test-devel subpackages
- Add post/preun scriptlets for global enablement

* Sun May 15 2016 Igor Gnatenko <i.gnatenko.brain@gmail.com> - 1.2.0-1
- Initial package
